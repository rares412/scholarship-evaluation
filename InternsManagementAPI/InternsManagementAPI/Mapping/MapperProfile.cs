﻿using AutoMapper;
using InternsManagementAPI.DomainModels;
using InternsManagementAPI.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.Mapping
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<InternView, InternDomain>();
            CreateMap<InternDomain, InternView>();
        }
    }
}
