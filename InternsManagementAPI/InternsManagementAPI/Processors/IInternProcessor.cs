﻿using InternsManagementAPI.DtoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.Processors
{
    public interface IInternProcessor
    {
        Task<List<InternView>> GetAllAsync();

        Task<InternView> GetInternAsync(string id);

        Task<InternView> AddAsync(InternView intern);

        Task<InternView> UpdateAsync(string id, InternView intern);

        Task<bool> DeleteAsync(string id);
    }
}
