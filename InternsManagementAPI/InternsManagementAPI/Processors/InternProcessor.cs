﻿using AutoMapper;
using InternsManagementAPI.DomainModels;
using InternsManagementAPI.DtoModels;
using InternsManagementAPI.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.Processors
{
    public class InternProcessor : IInternProcessor
    {
        private readonly IInternReporitory _internRepository;
        private readonly IMapper _mapper;

        public InternProcessor(IMapper mapper, IInternReporitory internReporitory)
        {
            _mapper = mapper;
            _internRepository = internReporitory;
        }

        public async Task<InternView> AddAsync(InternView intern)
        {
            var internDomain = _mapper.Map<InternDomain>(intern);
            return _mapper.Map<InternView>(await _internRepository.AddAsync(internDomain));
        }

        public async Task<bool> DeleteAsync(string id)
        {
            return await _internRepository.DeleteAsync(id);
        }

        public async Task<List<InternView>> GetAllAsync()
        {
            return _mapper.Map<List<InternView>>((await _internRepository.GetAllAsync()).ToList());
        }

        public async Task<InternView> GetInternAsync(string id)
        {
            return _mapper.Map<InternView>(await _internRepository.GetInternAsync(id));
        }

        public async Task<InternView> UpdateAsync(string id, InternView intern)
        {
            var internDomain = _mapper.Map<InternDomain>(intern);
            return _mapper.Map<InternView>(await _internRepository.UpdateAsync(id, internDomain));
        }
    }
}
