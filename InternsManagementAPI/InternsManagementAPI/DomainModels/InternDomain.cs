﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.DomainModels
{
    public class InternDomain
    {
        [BsonId]public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Age { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
}
