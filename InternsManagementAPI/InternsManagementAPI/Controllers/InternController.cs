﻿using InternsManagementAPI.DtoModels;
using InternsManagementAPI.Processors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class InternController : ControllerBase
    {
        private readonly IInternProcessor _internProcessor;

        public InternController(IInternProcessor internProcessor)
        {
            _internProcessor = internProcessor;
        }


        /// <summary>
        /// Gets all Interns
        /// </summary>
        /// <returns>Code 200</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            return Ok(await _internProcessor.GetAllAsync());
        }

        /// <summary>
        /// Gets a intern that has matching id
        /// </summary>
        /// <param name="id">id of the desired intern</param>
        /// <returns>Code 200</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInternAsync(string id)
        {
            return Ok(await _internProcessor.GetInternAsync(id));
        }

        /// <summary>
        /// Adds a new intern 
        /// </summary>
        /// <param name="intern">Intern information</param>
        /// <returns>Code 201 or Code 400 if no information is given</returns>
        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody] InternView intern)
        {
            if(intern==null)
            {
                return BadRequest();
            }
            var createdIntern = await _internProcessor.AddAsync(intern);

            return CreatedAtRoute(createdIntern, createdIntern);
        }


        /// <summary>
        /// Updates intern information
        /// </summary>
        /// <param name="id">id of the intern</param>
        /// <param name="intern">information that will be updated</param>
        /// <returns>Code 200,Code 400 if no information was given or Code 201 is a new intern was created</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(string id,[FromBody] InternView intern)
        {
            if(intern==null)
            {
                return BadRequest();
            }

            var result = await _internProcessor.UpdateAsync(id, intern);
            if(intern!=null)
            {
                return Ok(intern);
            }

            return CreatedAtRoute(result, result);
        }

        /// <summary>
        /// Deletes an intern
        /// </summary>
        /// <param name="id">id of the intern that will be deleted</param>
        /// <returns>Code 200 or Code 404 if the id does not match </returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(string id)
        {
            var result = await _internProcessor.DeleteAsync(id);
            if(result)
            {
                return Ok();
            }
            return NotFound();
        }
    }
}
