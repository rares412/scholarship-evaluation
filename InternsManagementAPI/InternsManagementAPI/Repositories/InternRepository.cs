﻿using InternsManagementAPI.DomainModels;
using InternsManagementAPI.Settings;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.Repositories
{
    public class InternRepository : IInternReporitory
    {
        private readonly IMongoCollection<InternDomain> _interns;

        public InternRepository(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _interns = database.GetCollection<InternDomain>(settings.InternCollectionName);

        }

        public async Task<InternDomain> AddAsync(InternDomain intern)
        {
            intern.Id = Guid.NewGuid().ToString();
            await _interns.InsertOneAsync(intern);
            return intern;
        }

        public async Task<bool> DeleteAsync(string id)
        {
            var result = await _interns.DeleteOneAsync(intern => intern.Id == id);
            if(!result.IsAcknowledged && result.DeletedCount==0)
            {
                return false;
            }
            return true;
        }

        public async  Task<List<InternDomain>> GetAllAsync()
        {
            return (await _interns.FindAsync(intern => true)).ToList();
        }

        public async Task<InternDomain> GetInternAsync(string id)
        {
            return (await _interns.FindAsync(intern => intern.Id == id)).FirstOrDefault();
        }

        public async Task<InternDomain> UpdateAsync(string id, InternDomain intern)
        {
            intern.Id = id;
            var result = await _interns.ReplaceOneAsync(intern => intern.Id == id, intern);
            if(!result.IsAcknowledged && result.ModifiedCount==0)
            {
                await _interns.InsertOneAsync(intern);
                return intern;
            }
            return intern;
        }
    }
}
