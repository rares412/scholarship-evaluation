﻿using InternsManagementAPI.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InternsManagementAPI.Repositories
{
    public interface IInternReporitory
    {
        Task<List<InternDomain>> GetAllAsync();

        Task<InternDomain> GetInternAsync(string id);

        Task<InternDomain> AddAsync(InternDomain intern);

        Task<InternDomain> UpdateAsync(string id, InternDomain intern);

        Task<bool> DeleteAsync(string id);


    }
}
