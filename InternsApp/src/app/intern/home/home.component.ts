import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  selectedSort: string = '';
  selectedFormat: string = '';

  constructor() {}

  ngOnInit(): void {}

  receiveSort(sort: string) {
    this.selectedSort = sort;
  }

  receiveFormat(format: string) {
    this.selectedFormat = format;
  }
}
