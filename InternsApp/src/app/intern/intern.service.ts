import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { first, map, Observable } from 'rxjs';
import { Intern } from './interfaces/intern';

@Injectable({
  providedIn: 'root',
})
export class InternService {
  readonly baseUrl = 'https://localhost:44322';
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  constructor(private httpClient: HttpClient) {}

  getInterns(): Observable<Intern[]> {
    return this.httpClient.get<Intern[]>(
      this.baseUrl + `/Intern`,
      this.httpOptions
    );
  }

  getInternsAscending(): Observable<Intern[]> {
    return this.httpClient
      .get<Intern[]>(this.baseUrl + `/Intern`, this.httpOptions)
      .pipe(
        map((interns) =>
          interns.sort((obj1, obj2) =>
            obj1.firstName + obj1.lastName < obj2.firstName + obj2.lastName
              ? -1
              : 1
          )
        )
      );
  }

  getInternsDescending(): Observable<Intern[]> {
    return this.httpClient
      .get<Intern[]>(this.baseUrl + `/Intern`, this.httpOptions)
      .pipe(
        map((interns) =>
          interns.sort((obj1, obj2) =>
            obj1.firstName + obj1.lastName > obj2.firstName + obj2.lastName
              ? -1
              : 1
          )
        )
      );
  }
  addIntern(intern: Intern) {
    return this.httpClient.post(
      this.baseUrl + '/Intern',
      intern,
      this.httpOptions
    );
  }

  getIntern(id: string): Observable<Intern> {
    return this.httpClient.get<Intern>(
      this.baseUrl + '/Intern/' + id,
      this.httpOptions
    );
  }

  deleteIntern(id: string): Observable<Intern> {
    let idUrl = '/Intern/' + id;
    return this.httpClient.delete<Intern>(this.baseUrl + idUrl);
  }

  editIntern(intern: Intern): Observable<Intern> {
    let idUrl = '/Intern/' + intern.id;
    return this.httpClient.put<Intern>(this.baseUrl + idUrl, intern);
  }
}
