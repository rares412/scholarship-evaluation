import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss'],
})
export class ToolsComponent implements OnInit {
  @Output() emitSort = new EventEmitter<string>();
  @Output() emitFormat = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  selectSort(sort: string) {
    this.emitSort.emit(sort);
  }

  selectFormat(format: string) {
    this.emitFormat.emit(format);
  }
}
