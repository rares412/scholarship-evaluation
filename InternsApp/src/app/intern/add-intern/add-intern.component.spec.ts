import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InternService } from '../intern.service';

import { AddInternComponent } from './add-intern.component';

describe('AddInternComponent', () => {
  let component: AddInternComponent;
  let fixture: ComponentFixture<AddInternComponent>;

  beforeEach(async () => {
    class internServiceStub {
      addIntern() {}
    }

    await TestBed.configureTestingModule({
      declarations: [AddInternComponent],
      providers: [
        { provides: InternService, useFactory: () => internServiceStub },
      ],
    }).compileComponents();
  });

  beforeEach(() => {});

  it('should create', () => {
    fixture = TestBed.createComponent(AddInternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should add a intern', () => {
    fixture = TestBed.createComponent(AddInternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const internFormGroupStub = new FormGroup({
      firstNameStub: new FormControl('test', [Validators.required]),
      lastNameStub: new FormControl('test', [Validators.required]),
      age: new FormControl('21', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
      ]),
      dateOfBirth: new FormControl('2000-01-01', Validators.required),
    });

    component.internFormGroup = internFormGroupStub;

    const service = TestBed.inject(InternService);
    const addInternSpy = spyOn(service, 'addIntern');

    component.addIntern();
    expect(addInternSpy).toHaveBeenCalledTimes(1);
    expect(component.intern.firstName).toEqual('test');
    expect(component.intern.lastName).toEqual('test');
    expect(component.intern.age).toEqual('21');
    expect(component.intern.dateOfBirth).toEqual(new Date('2000-01-01'));
  });
});
