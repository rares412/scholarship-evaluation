import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Intern } from '../interfaces/intern';
import { InternService } from '../intern.service';

@Component({
  selector: 'app-add-intern',
  templateUrl: './add-intern.component.html',
  styleUrls: ['./add-intern.component.scss'],
})
export class AddInternComponent implements OnInit {
  intern: Intern = {
    id: '',
    firstName: '',
    lastName: '',
    age: '',
    dateOfBirth: new Date(),
  };

  internFormGroup: FormGroup = new FormGroup({});

  constructor(
    private internService: InternService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.internFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      age: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      dateOfBirth: ['', [Validators.required]],
    });
  }

  addIntern() {
    this.intern.firstName = this.internFormGroup.value.firstName;
    this.intern.lastName = this.internFormGroup.value.lastName;
    this.intern.age = this.internFormGroup.value.age;
    this.intern.dateOfBirth = this.internFormGroup.value.dateOfBirth;
    this.internService.addIntern(this.intern).subscribe();
  }
}
