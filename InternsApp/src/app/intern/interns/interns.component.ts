import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { switchMap } from 'rxjs';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { Intern } from '../interfaces/intern';
import { InternService } from '../intern.service';

@Component({
  selector: 'app-interns',
  templateUrl: './interns.component.html',
  styleUrls: ['./interns.component.scss'],
})
export class InternsComponent implements OnInit, OnChanges {
  interns: Intern[] = [];

  @Input() selectedSort: string = '';
  @Input() selectedFormat: string = '';

  constructor(private internService: InternService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.internService
      .getInterns()
      .subscribe((interns) => (this.interns = interns));
  }

  ngOnChanges(): void {
    if (this.selectedSort == 'a') {
      this.internService
        .getInternsAscending()
        .subscribe((interns) => (this.interns = interns));
    }
    if (this.selectedSort == 'd') {
      this.internService
        .getInternsDescending()
        .subscribe((interns) => (this.interns = interns));
    }
    if (this.selectedSort == 'r') {
      this.internService
        .getInterns()
        .subscribe((interns) => (this.interns = interns));
    }
  }

  deleteIntern(id: string) {
    this.internService
      .deleteIntern(id)
      .pipe(switchMap((x) => this.internService.getInterns()))
      .subscribe((interns) => (this.interns = interns));
  }

  openDialog(intern: Intern): void {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '500px',
      height: '500px',
      data: {
        firstName: intern.firstName,
        lastName: intern.lastName,
        age: intern.age,
        dateOfBirth: intern.dateOfBirth,
        id: intern.id,
      },
    });

    dialogRef
      .afterClosed()
      .pipe(switchMap((_) => this.internService.getInterns()))
      .subscribe((interns) => (this.interns = interns));
  }
}
