export interface Intern {
  id: string;
  firstName: string;
  lastName: string;
  age: string;
  dateOfBirth: Date;
}
