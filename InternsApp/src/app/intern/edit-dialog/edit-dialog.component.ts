import { Component, Inject, OnInit } from '@angular/core';
import { inject } from '@angular/core/testing';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Intern } from '../interfaces/intern';
import { InternService } from '../intern.service';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss'],
})
export class EditDialogComponent implements OnInit {
  internFormGroup: FormGroup = new FormGroup({});
  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public intern: Intern,
    private formBuilder: FormBuilder,
    private internService: InternService
  ) {}

  ngOnInit(): void {
    this.internFormGroup = this.formBuilder.group({
      firstName: [this.intern.firstName, [Validators.required]],
      lastName: [this.intern.lastName, [Validators.required]],
      age: [
        this.intern.age,
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
      dateOfBirth: [this.intern.dateOfBirth, [Validators.required]],
    });
    console.log(this.internFormGroup);
  }

  editIntern() {
    this.intern.firstName = this.internFormGroup.value.firstName;
    this.intern.lastName = this.internFormGroup.value.lastName;
    this.intern.age = this.internFormGroup.value.age;
    this.intern.dateOfBirth = this.internFormGroup.value.dateOfBirth;
    this.internService.editIntern(this.intern).subscribe();
    this.dialogRef.close();
  }
}
